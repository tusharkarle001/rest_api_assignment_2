const mongoose = require("mongoose");
require("../connection/connect.js");

const customerSchema = mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    age: {
        type: Number,
        required:true,
    }
});

const customer = new mongoose.model("Customer", customerSchema);

module.exports = customer;