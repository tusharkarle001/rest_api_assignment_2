const express = require("express");
require("../connection/connect.js");
const router = express.Router();
const customer = require("../models/model.js");

router.use(express.json())



router.get("/", async (req, res) => {
    try {
        res.send("homepage")
    } catch (error) {
        res.send(error);
    }
});

router.post("/customer", async (req, res) => {
    try {
        const newCustomer = customer(req.body);
        const createCustomer = await newCustomer.save();
        res.status(200).send(createCustomer);
        
    } catch (error) {
        res.status(404).send(error);
    }
});
 
router.get("/customer", async (req, res) => {
    try {
    
        const getCustomers = await customer.find();
        if (!getCustomers) {
            res.status(404).send("Customer not found");
        }
        else { 
            res.status(200).send(getCustomers);
        }

    } catch (error) {
        res.send(error);
    }
});

router.get("/customer/:name", async (req, res)=>{
    try {
        const reqName = req.params.name;
        const reqCustomer = await customer.find({name:reqName});
        if (!reqCustomer.length) {
            res.status(404).send("Customer not Found");
        }
        else { 
            res.status(200).send(reqCustomer);
        }

    } catch (err) {
        res.status(404).send(err);
    }
});

router.patch("/customer/:id", async (req,res) => {
    try {
        const givenId = req.params.id;
        const updateCustomer = await customer.findByIdAndUpdate(givenId, req.body, {
            new: true,
        });
        
        res.send(updateCustomer);
    } catch (error) {
        res.status(400).send(error);
    }
});
 

router.delete("/customer/:id", async (req, res) => {
    try {
        const givenId = req.params.id;
        const deleteCustomer = await customer.findByIdAndDelete(givenId);
        if (!deleteCustomer) {
            res.status(404).send("Customer not found for Deleting");
        }
        else {
            res.send(deleteCustomer);
        }
    } catch (error) {
        res.send(error);
    }
 });


module.exports = router;